package com.sshih.poc;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder stores the result of a password rule validation.
 *
 * @author  Steve Shih
 */
public class ValidationResult {

	boolean isValid;
	
	final List<ValidationResultDetail> details = new ArrayList<ValidationResultDetail>();

	
	public ValidationResult() {
		this(true);
	}

	public ValidationResult(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public List<ValidationResultDetail> getDetails() {
		return details;
	}

    public void addDetail(ValidationResultDetail detail) {
    	details.add(detail);
    }
	
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		
		builder.append(MessageFormat.format("validate result is {0}\n", isValid()));
		details.forEach(d -> {	
			builder.append(MessageFormat.format("ERROR: {0}\n", d.getErrorDesc()));
		});
		
		
		return builder.toString();
	}

}
