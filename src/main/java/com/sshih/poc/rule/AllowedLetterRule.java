package com.sshih.poc.rule;

import java.text.MessageFormat;

import com.sshih.poc.ValidationResult;
import com.sshih.poc.ValidationResultDetail;

/**
 * Rule to check is a password consists of a mixture of lower case letters and numerical digits only, with at least one of each.
 *
 * @author  Steve Shih
 */
public class AllowedLetterRule implements ValidationRule {

	private static final String ERROR_CODE = "ERR_INVALID_CHAR";
	private static final String ERROR_NO_LOWERCASE = "Password should contain at least one lowercase character";
	private static final String ERROR_NO_NUMBER = "Password should contain at least one number digit";
	private static final String ERROR_INVALID_CHAR = "Password should only consist of lowercase letters or number digits";

	@Override
	public ValidationResult validate(final String password) {
		ValidationResult result = new ValidationResult();
		result.setValid(true);

		boolean invalidCharFlag = false;
		boolean numFlag = false;
		boolean lowerCaseFlag = false;

		char[] text = password.toCharArray();

		for (char c : text) {
			if (c >= 'a' && c <= 'z') {
				lowerCaseFlag = true;
			} else if (c >= '0' && c <= '9') {
				numFlag = true;
			} else {
				invalidCharFlag = true;
			}
		}

		if (lowerCaseFlag == false) {
			ValidationResultDetail detail = new ValidationResultDetail(ERROR_CODE,
					MessageFormat.format(ERROR_NO_LOWERCASE, null));
			result.setValid(false);
			result.addDetail(detail);
		}
		if (numFlag == false) {
			ValidationResultDetail detail = new ValidationResultDetail(ERROR_CODE,
					MessageFormat.format(ERROR_NO_NUMBER, null));
			result.setValid(false);
			result.addDetail(detail);
		}
		if (invalidCharFlag == true) {
			ValidationResultDetail detail = new ValidationResultDetail(ERROR_CODE,
					MessageFormat.format(ERROR_INVALID_CHAR, null));
			result.setValid(false);
			result.addDetail(detail);
		}
		return result;
	}

}
