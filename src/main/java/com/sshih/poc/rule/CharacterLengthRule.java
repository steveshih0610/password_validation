package com.sshih.poc.rule;

import java.text.MessageFormat;

import com.sshih.poc.ValidationResult;
import com.sshih.poc.ValidationResultDetail;

/**
 * Validates whether a password length is within the specified range. Default range is between 5 to 12.
 *
 * @author  Steve Shih
 */
public class CharacterLengthRule implements ValidationRule {

	private static final String ERROR_CODE = "ERR_CHAR_LENGTH";
	private static final String ERROR_LESS_THAN_MIN = "Password length should be not less than {0}";
	private static final String ERROR_LARGER_THAN_MAX = "Password length should be not larger than {0}";

	private final int minCharLength;
	private final int maxCharLength;

	public CharacterLengthRule() {
		this(5, 12);
	}

	  /**
	   * Sets the minimum and maximum character length
	   *
	   * @param  minCharLength  allowed minimum character length
	   * @param  maxCharLength  allowed maxinum character length
	   */	
	public CharacterLengthRule(int minCharLength, int maxCharLength) {
		if( minCharLength> maxCharLength) {
	      throw new IllegalArgumentException("minimum character length must be greater than maxinum character length");
	    }
		this.minCharLength = minCharLength;
		this.maxCharLength = maxCharLength;
	}

	@Override
	public ValidationResult validate(final String password) {
		ValidationResult result = new ValidationResult();
		result.setValid(true);

		if (password.length() < minCharLength) {
			ValidationResultDetail detail = new ValidationResultDetail(ERROR_CODE,
					MessageFormat.format(ERROR_LESS_THAN_MIN, minCharLength));
			result.setValid(false);
			result.addDetail(detail);

		}

		if (password.length() > maxCharLength) {
			ValidationResultDetail detail = new ValidationResultDetail(ERROR_CODE,
					MessageFormat.format(ERROR_LARGER_THAN_MAX, maxCharLength));
			result.setValid(false);
			result.addDetail(detail);

		}

		return result;
	}

}
