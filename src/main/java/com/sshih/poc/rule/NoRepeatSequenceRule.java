package com.sshih.poc.rule;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sshih.poc.ValidationResult;
import com.sshih.poc.ValidationResultDetail;

/**
 * Rule to check is a password not contain any sequence of characters
 * immediately followed by the same sequence. The default minimum character
 * sequence length to check is 2.
 *
 * @author Steve Shih
 */
public class NoRepeatSequenceRule implements ValidationRule {

	private static final String ERROR_CODE = "ERR_NO_REPEAT_SEQ";
	private static final String ERROR_DESC = "Found repeated sequence at position: {0} for word:{1}";
	private final int minWordLength;

	public NoRepeatSequenceRule() {
		minWordLength = 2;
	}

	/**
	 * Sets the minimum character sequence length to check
	 *
	 * @param minWordLength minimum character length to check
	 */
	public NoRepeatSequenceRule(int minWordLength) {
		this.minWordLength = minWordLength;
	}

	public int getMinWordLength() {
		return minWordLength;
	}

	public ValidationResult validate(final String password) {

		ValidationResult result = new ValidationResult();
		result.setValid(true);

		/*
		 * find all the words from the string and check their positions put each word in
		 * a hashmap with the word as key and the word's next position as value if a
		 * word found in the hashmap than check if they have the same positions if the
		 * positions are not the same than replace the position with the new one
		 */

		for (int i = minWordLength; i < password.length(); i++) {
			Map<String, Integer> wordMap = new LinkedHashMap<String, Integer>();
			for (int j = 0; j + i <= password.length(); j++) {
				String word = password.substring(j, j + i);
				if (wordMap.get(word) == null) {
					wordMap.put(word, j + i);
				} else {
					int previousLoc = wordMap.get(word);
					if (previousLoc == j) {
						ValidationResultDetail detail = new ValidationResultDetail(ERROR_CODE,
								MessageFormat.format(ERROR_DESC, j, word));
						result.setValid(false);
						result.addDetail(detail);
					} else {
						wordMap.put(word, j + i);
					}
				}
			}

		}
		return result;

	}

}
