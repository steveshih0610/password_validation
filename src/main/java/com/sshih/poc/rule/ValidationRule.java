package com.sshih.poc.rule;

import com.sshih.poc.ValidationResult;

/**
 * Interface for password validation rules.
 *
 * @author  Steve Shih
 */
public interface ValidationRule {

	  /**
	   * Validates the input password string against the rule.
	   *
	   * @param  password  to verify
	   *
	   * @return  ValidationResult the validation result
	   *
	   */
	ValidationResult validate(final String password);
}

