package com.sshih.poc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

import com.sshih.poc.rule.AllowedLetterRule;
import com.sshih.poc.rule.CharacterLengthRule;
import com.sshih.poc.rule.NoRepeatSequenceRule;

@Configuration
@ComponentScans({ 
	  @ComponentScan(basePackages = "com.sshih.poc.service")
	})
public class Config {

	@Bean
	AllowedLetterRule allowedLetterRule() {
		return new AllowedLetterRule();
	}
	
	@Bean
	CharacterLengthRule characterLengthRule() {
		return new CharacterLengthRule();
	}

	@Bean
	NoRepeatSequenceRule noRepeatSequenceRule() {
		return new NoRepeatSequenceRule();
	}
	
}
