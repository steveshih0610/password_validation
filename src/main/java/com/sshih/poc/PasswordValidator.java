package com.sshih.poc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.tomcat.util.digester.Rule;

import com.sshih.poc.rule.AllowedLetterRule;
import com.sshih.poc.rule.CharacterLengthRule;
import com.sshih.poc.rule.NoRepeatSequenceRule;
import com.sshih.poc.rule.ValidationRule;

/**
 * The main class for check multiple password rules against a candidate
 * password.
 *
 * @author Steve Shih
 */
public class PasswordValidator {

	private final List<ValidationRule> appliedRules;

	/**
	 * Creates a new password validator.
	 *
	 * @param rules to validate
	 */
	public PasswordValidator(final ValidationRule... rules) {
		this(Arrays.asList(rules));
	}

	/**
	 * Creates a new password validator.
	 *
	 * @param rules to validate
	 */
	public PasswordValidator(final List<ValidationRule> rules) {
		appliedRules = rules;
	}

	/**
	 * Validates the input password against all the rules in this validator.
	 *
	 * @param password to validate
	 *
	 * @return ValidationResult result
	 */
	public ValidationResult validate(final String password) {

		ValidationResult result = new ValidationResult();
		for (ValidationRule rule : appliedRules) {
			ValidationResult rr = rule.validate(password);
			if (!rr.isValid()) {
				result.setValid(false);
				result.getDetails().addAll(rr.getDetails());
			}
		}
		return result;
	}

	public static void main(final String[] args) throws Exception {
		AllowedLetterRule arule = new AllowedLetterRule();
		CharacterLengthRule crule = new CharacterLengthRule();
		NoRepeatSequenceRule nrule = new NoRepeatSequenceRule();

		final List<ValidationRule> rules = new ArrayList<ValidationRule>();
		String password = null;

		try {
			if (args.length < 1) {
				throw new ArrayIndexOutOfBoundsException();
			}
			for (int i = 0; i < args.length; i++) {
				if ("-a".equals(args[i])) {
					rules.add(arule);
				} else if ("-c".equals(args[i])) {
					rules.add(crule);
				} else if ("-n".equals(args[i])) {
					rules.add(nrule);
				} else if ("--all".equals(args[i])) {
					rules.add(arule);
					rules.add(crule);
					rules.add(nrule);
				} else if ("-h".equals(args[i])) {
					throw new ArrayIndexOutOfBoundsException();
				} else {
					password = args[i];
				}

			}
			if (password == null) {
				throw new ArrayIndexOutOfBoundsException();
			} else {
				if(rules.size()==0) {
					rules.add(arule);
					rules.add(crule);
					rules.add(nrule);
				}
				PasswordValidator validator = new PasswordValidator(rules);
				ValidationResult result = validator.validate(password);
				System.out.println(result);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		      System.out.println("Usage: java -jar PasswordValidation.jar <options> <password>");
		      System.out.println();
		      System.out.println("Options:");
		      System.out.println("    -a test password consists of lowercase letters and numerical digits only, with at least one of each");
		      System.out.println("    -c test password length is between 5 and 12 characters in length");
		      System.out.println("    -n test password does not contain any sequence of characters immediately followed by the same sequence");
		      System.out.println("    --all apply all the tests");
		      System.out.println("    -h print this help message");
		      System.exit(1);
		}
	}
}
