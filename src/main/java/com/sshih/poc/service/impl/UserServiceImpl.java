package com.sshih.poc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sshih.poc.PasswordValidator;
import com.sshih.poc.ValidationResult;
import com.sshih.poc.rule.AllowedLetterRule;
import com.sshih.poc.rule.CharacterLengthRule;
import com.sshih.poc.rule.NoRepeatSequenceRule;
import com.sshih.poc.rule.ValidationRule;
import com.sshih.poc.service.UserService;
import com.sshih.poc.util.SpringContextUtil;

@Service
public class UserServiceImpl implements UserService {

	PasswordValidator validator;

	@Autowired
	SpringContextUtil springContextUtil;

	@Value("${password.validation.rules}")
	String rules;
	
	/*
	public UserServiceImpl(@Value("${password.validation.rules}") String rules) {
		List<ValidationRule> ruleList = new ArrayList<ValidationRule>();
		StringTokenizer st = new StringTokenizer(rules, ",");
		while(st.hasMoreElements()){
			ValidationRule arule = (ValidationRule) springContextUtil.getBean(st.nextToken());
			ruleList.add(arule);
			validator = new PasswordValidator(ruleList);
		}
	}
	*/
	

	@PostConstruct
	public void init() {

		List<ValidationRule> ruleList = new ArrayList<ValidationRule>();
		StringTokenizer st = new StringTokenizer(rules, ",");
		while(st.hasMoreElements()){
			ValidationRule arule = (ValidationRule) springContextUtil.getBean(st.nextToken());
			ruleList.add(arule);
			validator = new PasswordValidator(ruleList);
		}
	}
	
	@Override
	public ValidationResult validatePassword(final String password) {

		return validator.validate(password);

	}

}
