package com.sshih.poc.service;

import com.sshih.poc.ValidationResult;

public interface UserService {

	ValidationResult validatePassword(final String password);
}
