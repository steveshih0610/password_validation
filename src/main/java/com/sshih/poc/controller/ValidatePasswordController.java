package com.sshih.poc.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sshih.poc.ValidationResult;
import com.sshih.poc.model.User;
import com.sshih.poc.service.UserService;

@Controller
@RequestMapping(value = "/passwordValidate")
public class ValidatePasswordController {

	@Autowired
	UserService userService;
	
    @ModelAttribute("user")
    public User User() {
        return new User();
    }
	
    @GetMapping
    public String showForm(Model model) {
    	//model.addAttribute("user",new User());
        return "test";
    }
    
    
    @PostMapping
    public String submitForm(@ModelAttribute("user") User user, Map<String,Object> map) {

    	ValidationResult result = userService.validatePassword(user.getPassword());
          map.put("valid", result.isValid());
          map.put("errors", result.getDetails());
        return "test";
    }
}
