package com.sshih.poc;

/**
 * Specify the error detail when a rule validation failed.
 *
 * @author  Steve Shih
 */
public class ValidationResultDetail {

	String errorCode;
	String errorDesc;
	public ValidationResultDetail(String errorCode, String errorDesc) {
		super();
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}
	
	
}
