package com.sshih.poc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sshih.poc.service.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(  classes = com.sshih.poc.PasswordValidationApplication.class)
public class SpringBootUserServiceTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(JUnitPasswordValidatorTest.class);

	@Autowired
	UserService userService;

	@Test
	public void testValid() {
		String password = "sdfsi55ab6gf";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertTrue(result.isValid);
		org.junit.Assert.assertEquals(0, result.details.size());
	}

	@Test
	public void testInvalidCharacter() {
		String password = "dsdfSi55V6gf";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}

	@Test
	public void testNoLowerCase() {
		String password = "48573483";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}

	@Test
	public void testNoNumberDigit() {
		String password = "csdsadj";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}

	@Test
	public void testInvalidLength() {
		String password = "dsdfsi55e6gf33";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}

	@Test
	public void testRepeatedSequence() {
		String password = "abcab65abab";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}

	@Test
	public void testRepeatedSequenceAndInvalidLength() {
		String password = "abcab6bfbd3835abab";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(2, result.details.size());
	}

	@Test
	public void testRepeatedSequenceAndInvalidLengthAndNoNumberDigit() {
		String password = "abcabgbfbdbtaabab";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(3, result.details.size());
	}

	@Test
	public void testRepeatedSequenceAndInvalidLengthAndNoNumberDigitAndInvalidCharacter() {
		String password = "abcabgbfbdXtaabab";
		ValidationResult result = userService.validatePassword(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(4, result.details.size());
	}

}
