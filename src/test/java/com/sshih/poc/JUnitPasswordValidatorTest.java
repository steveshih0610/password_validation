package com.sshih.poc;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sshih.poc.rule.AllowedLetterRule;
import com.sshih.poc.rule.CharacterLengthRule;
import com.sshih.poc.rule.NoRepeatSequenceRule;

public class JUnitPasswordValidatorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(JUnitPasswordValidatorTest.class);

	PasswordValidator validator;
	
	@Before
	public void setUp() throws Exception {
		AllowedLetterRule arule = new AllowedLetterRule();
		CharacterLengthRule crule = new CharacterLengthRule();
		NoRepeatSequenceRule nrule = new NoRepeatSequenceRule();
		validator = new PasswordValidator(arule,crule,nrule);
	}

	@Test
	public void testValid() {
		String password = "sdfsi55ab6gf";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertTrue(result.isValid);
		org.junit.Assert.assertEquals(0, result.details.size());
	}
	
	
	@Test
	public void testInvalidCharacter() {
		String password = "dsdfSi55V6gf";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}

	@Test
	public void testNoLowerCase() {
		String password = "48573483";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}
	
	@Test
	public void testNoNumberDigit() {
		String password = "csdsadj";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}
	
	
	
	@Test
	public void testInvalidLength() {
		String password = "dsdfsi55e6gf33";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}
	
	@Test
	public void testRepeatedSequence() {
		String password = "abcab65abab";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(1, result.details.size());
	}
	
	@Test
	public void testRepeatedSequenceAndInvalidLength() {
		String password = "abcab6bfbd3835abab";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(2, result.details.size());
	}
	
	@Test
	public void testRepeatedSequenceAndInvalidLengthAndNoNumberDigit() {
		String password = "abcabgbfbdbtaabab";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(3, result.details.size());
	}

	@Test
	public void testRepeatedSequenceAndInvalidLengthAndNoNumberDigitAndInvalidCharacter() {
		String password = "abcabgbfbdXtaabab";
		ValidationResult result = validator.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid);
		org.junit.Assert.assertEquals(4, result.details.size());
	}
	
}
