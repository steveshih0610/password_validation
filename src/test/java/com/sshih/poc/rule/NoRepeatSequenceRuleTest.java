package com.sshih.poc.rule;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sshih.poc.ValidationResult;

public class NoRepeatSequenceRuleTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(NoRepeatSequenceRuleTest.class);

    
	NoRepeatSequenceRule rule;
	
	@Before
	public void setUp() throws Exception {
		rule = new NoRepeatSequenceRule();
	}

	@Test
	public void testValid() {
		String password = "14394ss4083409";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertTrue(result.isValid());
		org.junit.Assert.assertEquals(0, result.getDetails().size());		
	}
	
	@Test
	public void testRepeatAtFront() {
		String password = "143143cfngmrgsfs";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}

	@Test
	public void testRepeatAtEnd() {
		String password = "cjhrfndfsdbd123123";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}

	@Test
	public void testRepeatAtMiddle() {
		String password = "cjhrfn123123cxcdnf";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}
	
	@Test
	public void testSpecifySequenceLengthValid() {
		rule = new NoRepeatSequenceRule(4);
		String password = "cjhrfn123123cxcdnf";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertTrue(result.isValid());
		org.junit.Assert.assertEquals(0, result.getDetails().size());			
	}
	
	@Test
	public void testSpecifySequenceLengthNotValid() {
		rule = new NoRepeatSequenceRule(4);
		String password = "cjhrfn12341234cxcdnf";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}
}
