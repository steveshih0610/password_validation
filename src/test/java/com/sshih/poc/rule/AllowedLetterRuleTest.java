package com.sshih.poc.rule;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sshih.poc.ValidationResult;

public class AllowedLetterRuleTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllowedLetterRuleTest.class);

    
	AllowedLetterRule rule;
	
	@Before
	public void setUp() throws Exception {
		rule = new AllowedLetterRule();
	}

	@Test
	public void testValid() {
		String password = "14394ss4083409";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertTrue(result.isValid());
		org.junit.Assert.assertEquals(0, result.getDetails().size());		
	}
	
	@Test
	public void testNoLowerCase() {
		String password = "143948394083409";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}

	@Test
	public void testNoNumverDigit() {
		String password = "cjhrfndfsdbd";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}
	
	@Test
	public void testInvalidCharacter() {
		String password = "cjhrSfndVfsdbd";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(2, result.getDetails().size());		
	}
	
}
