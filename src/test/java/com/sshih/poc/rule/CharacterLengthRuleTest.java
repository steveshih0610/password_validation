package com.sshih.poc.rule;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sshih.poc.ValidationResult;

public class CharacterLengthRuleTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CharacterLengthRuleTest.class);

    
	CharacterLengthRule rule;
	
	@Before
	public void setUp() throws Exception {
		rule = new CharacterLengthRule();
	}

	@Test
	public void testValid() {
		String password = "14399";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertTrue(result.isValid());
		org.junit.Assert.assertEquals(0, result.getDetails().size());		
	}
	
	@Test
	public void testLengthLess() {
		String password = "1433";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}

	@Test
	public void testLengthOver() {
		String password = "cjhrfndcdvdfddfsdbd";
		ValidationResult result = rule.validate(password);
		LOGGER.debug(result.toString());
		org.junit.Assert.assertFalse(result.isValid());
		org.junit.Assert.assertEquals(1, result.getDetails().size());		
	}
	
	
}
